import axios from "axios";

function setSessionToken(token) {
  axios.defaults.headers.common["Authorization"] = "Bearer " + token;
  sessionStorage.setItem("token", "Bearer " + token);
}

function setLocalToken(token) {
  axios.defaults.headers.common["Authorization"] = "Bearer " + token;
  localStorage.setItem("token", "Bearer " + token);
}

function getToken() {
  return sessionStorage.getItem("token") || localStorage.getItem("token");
}

function clearToken() {
  axios.defaults.headers.common["Authorization"] = null;
  sessionStorage.removeItem("token");
  localStorage.removeItem("token");
}

export { setSessionToken, setLocalToken, getToken, clearToken };
